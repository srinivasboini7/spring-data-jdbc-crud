-- Inserting sample data into the Account table
INSERT INTO Account (account_number, balance, account_type)
VALUES ('A001', 1000.00, 'Savings');

INSERT INTO Account (account_number, balance, account_type)
VALUES ('A002', 500.00, 'Checking');

WITH RECURSIVE row_numbers(n) AS (
  SELECT 3
  UNION ALL
  SELECT n + 1 FROM row_numbers WHERE n + 1 <= 100
)
INSERT INTO Account (account_number, balance, account_type)
SELECT
  CONCAT('A', LPAD(n, 3, '0')),
  FLOOR(RAND() * 100000) + 1000,
  CASE FLOOR(RAND() * 2)
    WHEN 0 THEN 'Savings'
    WHEN 1 THEN 'Checking'
  END
FROM row_numbers;



-- Inserting sample data into the Transaction table
INSERT INTO Transaction (account_id, amount, transaction_type, transaction_date)
VALUES (1, 500.00, 'Deposit', '2023-05-01');

INSERT INTO Transaction (account_id, amount, transaction_type, transaction_date)
VALUES (2, 200.00, 'Withdrawal', '2023-05-03');

WITH RECURSIVE row_numbers(n) AS (
  SELECT 3
  UNION ALL
  SELECT n + 1 FROM row_numbers WHERE n + 1 <= 100
)
INSERT INTO Transaction (account_id, amount, transaction_type, transaction_date)
SELECT
  ROUND(RAND() * 100) + 1,
  ROUND(RAND() * 1000) + 1,
  CASE FLOOR(RAND() * 2)
    WHEN 0 THEN 'Deposit'
    WHEN 1 THEN 'Withdrawal'
  END,
  DATEADD('DAY', ROUND(RAND() * 365), '2023-01-01')
FROM row_numbers;