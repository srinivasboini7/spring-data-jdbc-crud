DROP ALL OBJECTS ;

-- Account table
CREATE TABLE Account (
  id INT AUTO_INCREMENT PRIMARY KEY,
  account_number VARCHAR(255) UNIQUE,
  balance DECIMAL(10, 2),
  account_type VARCHAR(255),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Transaction table
CREATE TABLE Transaction (
  id INT AUTO_INCREMENT PRIMARY KEY,
  account_id INT,
  amount DECIMAL(10, 2),
  transaction_type VARCHAR(255),
  transaction_date DATE,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (account_id) REFERENCES Account(id)
);