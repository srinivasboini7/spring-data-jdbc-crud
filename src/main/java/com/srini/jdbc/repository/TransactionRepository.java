package com.srini.jdbc.repository;

import com.srini.jdbc.model.Transaction;
import org.springframework.data.annotation.Id;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
 * The interface Transaction repository.
 */
@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Integer> {

    /**
     * Update.
     *
     * @param id     the id
     * @param amount the amount
     */
    @Modifying
    @Query("update Transaction set amount=:amount where id=:id")
    void update(@Param("id") Integer id, @Param("amount")BigDecimal amount) ;
}
