package com.srini.jdbc.repository;

import com.srini.jdbc.model.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface Account repository.
 */
@Repository
public interface AccountRepository extends PagingAndSortingRepository<Account, Integer> {

    /**
     * Find by account number account.
     *
     * @param accountNumber the account number
     * @return the account
     */
    public Account findByAccountNumber(String accountNumber) ;
}
