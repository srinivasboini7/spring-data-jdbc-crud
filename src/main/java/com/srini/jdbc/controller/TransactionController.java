package com.srini.jdbc.controller;

import com.srini.jdbc.model.Transaction;
import com.srini.jdbc.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.OptionalInt;

/**
 * The type Transaction controller.
 */
@RestController
@RequestMapping("/transaction")
@RequiredArgsConstructor
public class TransactionController {

    private final TransactionRepository transactionRepository ;


    /**
     * Get all transactions iterable.
     *
     * @return the iterable
     */
    @GetMapping("/getAll")
    public Iterable<Transaction> getAllTransactions(){
        return transactionRepository.findAll() ;
    }

    /**
     * Gets transaction by id.
     *
     * @param id the id
     * @return the transaction by id
     */
    @GetMapping("/get/{id}")
    public Optional<Transaction> getTransactionById(@PathVariable(name="id") Integer id){
        return transactionRepository.findById(id) ;
    }

    /**
     * Save transaction transaction.
     *
     * @param transaction the transaction
     * @return the transaction
     */
    @PostMapping("/save")
    public Transaction saveTransaction(@RequestBody Transaction transaction){
        return transactionRepository.save(transaction) ;
    }


    /**
     * Update amount.
     *
     * @param id     the id
     * @param amount the amount
     */
    @PutMapping("/update")
    public void updateAmount(@RequestParam(name="id") Integer id, @RequestParam(name="amount")BigDecimal amount){
        transactionRepository.update(id, amount);
    }
}
