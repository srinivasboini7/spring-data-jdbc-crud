package com.srini.jdbc.controller;


import com.srini.jdbc.model.Account;
import com.srini.jdbc.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Pageable;

/**
 * The type Accounts controller.
 */
@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountsController {

    private final AccountRepository accountRepository ;

    /**
     * Get all accounts iterable.
     *
     * @return the iterable
     */
    @GetMapping("/getAll")
    public Iterable<Account> getAllAccounts(){
        return accountRepository.findAll(Sort.by(Sort.Order.asc("accountNumber"))) ;
    }

    /**
     * Gets account.
     *
     * @param accountNumber the account number
     * @return the account
     */
    @GetMapping("/get/{number}")
    public Account getAccount(@PathVariable(name = "number") String accountNumber){
        return  accountRepository.findByAccountNumber(accountNumber) ;
    }


    /**
     * Gets accountry by page.
     *
     * @param page the page
     * @return the accountry by page
     */
    @GetMapping("/getByPage")
    public Iterable<Account> getAccountryByPage(@RequestParam(name="page") Integer page){

        return accountRepository.findAll(PageRequest.of(1,page)) ;
    }
}
