package com.srini.jdbc.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * The type Account.
 */
public record Account(@Id Integer id, String accountNumber, BigDecimal balance, String accountType, LocalDateTime createdAt, LocalDateTime updatedAt)  implements Persistable<Integer> {

    /**
     * Returns the id of the entity.
     *
     * @return the id. Can be {@literal null}.
     */
    @Override
    public Integer getId() {
        return id();
    }

    /**
     * Returns if the {@code Persistable} is new or was persisted already.
     *
     * @return if {@literal true} the object is new.
     */
    @Override
    public boolean isNew() {
        return true;
    }
}
