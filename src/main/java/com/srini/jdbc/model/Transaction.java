package com.srini.jdbc.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * The type Transaction.
 */
public record Transaction(@Id Integer id, Integer accountId, BigDecimal amount, String transactionType,
                          LocalDate transactionDate, LocalDateTime createdAt, LocalDateTime updatedAt) implements Persistable<Integer> {

    /**
     * Returns the id of the entity.
     *
     * @return the id. Can be {@literal null}.
     */
    @Override
    public Integer getId() {
        return id();
    }

    /**
     * Returns if the {@code Persistable} is new or was persisted already.
     *
     * @return if {@literal true} the object is new.
     */
    @Override
    public boolean isNew() {
        return true;
    }
}