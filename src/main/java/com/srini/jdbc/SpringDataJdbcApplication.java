package com.srini.jdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The type Spring data jdbc application.
 */
@SpringBootApplication
public class SpringDataJdbcApplication {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
		SpringApplication.run(SpringDataJdbcApplication.class, args);
	}

}
