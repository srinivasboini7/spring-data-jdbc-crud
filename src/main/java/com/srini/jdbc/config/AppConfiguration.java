package com.srini.jdbc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.repository.config.AbstractJdbcConfiguration;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionManager;

import javax.sql.DataSource;

/**
 * The type App configuration.
 */
@Configuration
@EnableJdbcRepositories(basePackages = "com.srini.jdbc.repository")
public class AppConfiguration extends AbstractJdbcConfiguration {

    /**
     * Named parameter jdbc operations named parameter jdbc operations.
     *
     * @param dataSource the data source
     * @return the named parameter jdbc operations
     */
    @Bean
    NamedParameterJdbcOperations namedParameterJdbcOperations(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    /**
     * Transaction manager transaction manager.
     *
     * @param dataSource the data source
     * @return the transaction manager
     */
    @Bean
    TransactionManager transactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}
