#!/bin/bash

set -e

IMAGE_NAME=$1

# Build the image
CONTAINER=$(buildah from gradle:7.0.2-jdk17)
buildah copy $CONTAINER . /app
buildah run $CONTAINER gradle build --no-daemon
buildah commit $CONTAINER $IMAGE_NAME
buildah rm $CONTAINER
